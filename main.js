const app = new Vue({
  el: "#app",
  data: {
    title: "Fruit list",
    fruits: [
      {
        name: "apple",
        count: 8,
      },
      {
        name: "orange",
        count: 10,
      },
      {
        name: "straberry",
        count: 15,
      },
      {
        name: "pear",
        count: 0,
      },
    ],
    newFruit: "",
    newCount: "",
    total: 0,
  },
  methods: {
    addFruit() {
      this.fruits.push({
        name: this.newFruit,
        count: this.newCount,
      });
      this.newFruit = "";
      this.newCount = "";
    },
  },
  computed: {
    sumFruit() {
      this.total = 0;
      for (fruit of this.fruits) {
        this.total += fruit.count;
      }
      return this.total;
    },
  },
});
